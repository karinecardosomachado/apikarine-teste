<?php
namespace App\Controllers;

use App\Services\ServiceInterface;

class BaseController implements ControllerInterface{
    
    private ServiceInterface $service;
    
    
    
    public function delete() {
        
    }

    public function get($id = null) {
        if ($id){
            return $this->service->getById($id);
        }else{
            return $this->service->getList();
        }
        
    }

    public function post() {
        
    }

    public function update() {
        
    }

}
