<?php
namespace App\Controllers;

interface ControllerInterface {
    public function get ($id = null);
    public function post();
    public function update();
    public function delete();
          
}