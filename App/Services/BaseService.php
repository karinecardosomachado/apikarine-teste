<?php

namespace App\Services;

use App\Dao\DaoInterface;

class BaseService implements ServiceInterface {

    protected DaoInterface $dao;

    function __construct(DaoInterface $dao) {
        $this->dado = $dao;
    }

    public function getById(int $id) {
        return $this->dao->select($id);
    }

    public function getList() {
        return $this->dao->selectAll();
    }

    public function insert(array $data) {
        return $this->dao->insert($data);
    }

    public function remove($id) {
        ;
    }

    public function update($id, array $data) {
        ;
    }

}
