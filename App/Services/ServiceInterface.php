<?php

namespace App\Services;

interface ServiceInterface {

    function getById($id);

    function getList();

    function remove($id);

    function update($id, array $data);

    function insert(array $data);
}
