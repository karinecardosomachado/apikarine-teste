<?php

namespace App\Services;

use App\Dao\PersonDao;

class PersonService extends BaseService {
    
    function __construct() {
        parent::__construct(new PersonDao());
    }
}