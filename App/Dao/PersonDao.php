<?php

namespace App\Dao;

class PersonDao extends BaseDao {

    protected function getColumnNames(): array {
        return[
            "name",
            "CPF",
            "Birth_Date",
            "Dad_id",
            "Mom_id"
        ];
    }

    protected function getModelClassName(): string {
        return Person::class;
    }

    protected function getTableName(): string {
        return "person";
    }

}
