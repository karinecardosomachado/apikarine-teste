<?php

namespace App\Dao;

use PDO;
use Exception;

class BaseDao implements DaoInterface {

    private $conn;

    private static function getConnPDO() {
        return new PDO(DBDRIVE . ': server=' . DBHOST . '; Database=' . DBNAME, DBUSER, DBPASS);
    }

    public function getConn(): PDO {
        return $this->conn;
    }

    function __construct() {
        $this->conn = self::getConnPDO();
    }

    public function insert(array $data) {
        
    }

    public function selectAll() {
        $sql = 'SELECT * FROM ' . $this->getTableName();
        $stmt = $this->getConn()->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, $this->getModelClassName());
        $result = $stmt->fetchAll();
        return $result;
    }

    public function select(int $id) {
        $sql = 'SELECT * FROM' . $this->getTableName() . 'WHERE' . $this->getPrimaryKey() . '=:id';

        $stmt = $this->getConn()->prepare($sql);
        $stmt->bindValue(':id', $id);
        $execute = $stmt->execute();

        if (!$execute) {
            throw new Exception(" Não foi possível executar a consulta no banco de dados");
        }
        $stmt->setFetchMode(PDO::FETCH_CLASS, $this->getModelClassName());
        $item = $stmt->fetch();

        if ($item) {
            return $item;
        } else {
            throw new Exception("Nenhum item encontrado!");
        }
    }

    protected function getPrimaryKey(): string {
        return "id";
    }

    protected abstract function getModelClassName(): string;

    protected abstract function getTableName(): string;

    protected abstract function getColumnNames(): array;
}
