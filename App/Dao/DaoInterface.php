<?php

namespace App\Dao;

interface DaoInterface {

    function select($id);

    function selectAll();

    function insert(array $data);
}
